//
//  File.swift
//  Project Sonic
//
//  Created by Peri on 10.11.21.
//

import Foundation

let EXAMPLE_USER_SPEEDS = [
    Measurement(value: 1.4, unit: UnitSpeed.metersPerSecond), //12:00
    Measurement(value: 2.8, unit: UnitSpeed.metersPerSecond), //6:00
    Measurement(value: 3, unit: UnitSpeed.metersPerSecond), //5:30
    Measurement(value: 3.7, unit: UnitSpeed.metersPerSecond), //4:30
    Measurement(value: 4.2, unit: UnitSpeed.metersPerSecond), //4:00
]

class IntervalGenerator {
    private let POINTS_PER_KM = 5.0

    // Divided by 5
    private let MIN_DURATION_SECONDS = [
        12, // 10K
        6, // 5K
        3, // 1K
        3, // Best Pace
    ]

    // Divided by 5
    private let MAX_DURATION_SECONDS = [
        12, // 10K
        12, // 5K
        12, // 1K
        6, // Best Pace
    ]

    private let INTENSITY_PACE_DISTRIBUTION = [
        // 10K  5K  1K  Best
        [50, 50, 0, 0], // Recovery
        [30, 60, 10, 0], // Light
        [0, 50, 40, 10], // Moderate
        [0, 40, 30, 30], // High
    ]

    public func generateRun(intensity: Intensity,
                            distance: Measurement<UnitLength>,
                            userPaces _: [Measurement<UnitSpeed>]) async -> [Interval] {
        let km = distance.converted(to: UnitLength.kilometers).value
        let points = Int(km * POINTS_PER_KM)

        let distribution = INTENSITY_PACE_DISTRIBUTION[intensity.rawValue]
        
        return try! await Task {
            return generateRun(distribution: distribution, points: points)
        }.result.get()
    }

    private func generateRun(distribution: [Int], points: Int) -> [Interval] {
        // 1. Generate a curve that represents paces. The integral is the distance
        // 2. Check whether the curve has a suitable (= not-too-noisy) shape, else repeat 1
        // 3. Improve the curve to match the distance by adding, removing or stretching/compressing the intervals
        
        var intervals: [Interval] = []
        repeat {
            intervals = generateIntervals(distribution: distribution, points: points)
        } while(!checkShapeOkay(intervals: intervals))
        
        intervals = finetuneIntervals(intervals: &intervals, points: points)
        
        return intervals
    }
    
    private func generateIntervals(distribution: [Int], points: Int) -> [Interval] {
        //Generate by random according to distribution from intensity?
        var intervals: [Interval] = []
        for (i,pace) in distribution.enumerated() {
            print("pace: \(pace)")
            for _ in 0..<(pace*points/100) {
                let duration = generateDuration(pace: Pace(rawValue: i+1) ?? Pace.Best)
                print("appending \(i+1) with duration \(duration)")
                intervals.append(Interval(pace: Pace(rawValue: i+1) ?? Pace.Best, duration: duration, recovery: duration))
            }
        }
        intervals = intervals.shuffled()
        for (i, interval) in intervals.enumerated() {
            //TODO intesity of last 5 minutes influences duration of recovery pace.
            // intesity is pace*duration. Intensity has different levels?
            intervals[i].recovery = interval.duration;
        }
        intervals[intervals.count-1].recovery.value = 0;
        return intervals
    }
    
    private func checkShapeOkay(intervals: [Interval]) -> Bool {
        //TODO Calculate average of jumps in intervals squared(?)
        //What is the threshold?
        return true;
    }
    
    private func finetuneIntervals(intervals: inout [Interval], points: Int) -> [Interval] {
        // When the distance vs. estimatedDistance is more than 10% off, add or remove intervals.
        // When the off-factor is less than 10% off, just add or remove some seconds.
        var distance = calcDistanceUserSpeed(intervals: intervals).converted(to: UnitLength.kilometers).value;
        let chosenDistance = Double(points)/POINTS_PER_KM;
        print("Printing distance before finetune:\(distance)\n");
        
        //Add random 5 seconds till estimated distance is close to chosenDistance
        while(chosenDistance - distance > 0.05) {
            print("chosenDistance: \(chosenDistance), distance: \(distance)")
            intervals = addToRandomInterval(intervals: &intervals);
            distance = calcDistanceUserSpeed(intervals: intervals).converted(to: UnitLength.kilometers).value
        }
        
        //If we overshot get below chosenDistance
        while(chosenDistance - distance < 0) {
            intervals = subFromRandomInterval(intervals: &intervals);
            distance = calcDistanceUserSpeed(intervals: intervals).converted(to: UnitLength.kilometers).value
        }
        print("Printing distance after finestune:\(distance)");
        return intervals;
    }
    
    private func addToRandomInterval(intervals: inout [Interval]) -> [Interval] {
        var modified = false;
        repeat {
            let rnd = Int.random(in:0 ... intervals.count-1)
            if (Int(intervals[rnd].duration.value) <= MAX_DURATION_SECONDS[intervals[rnd].pace.rawValue-1]*5 - 5) {
                intervals[rnd].duration.value += 5;
                modified = true;
            }
        } while(!modified)
        return intervals;
    }
    
    private func subFromRandomInterval(intervals: inout [Interval]) -> [Interval] {
        var modified = false;
        repeat {
            let rnd = Int.random(in:0 ... intervals.count-1)
            if (Int(intervals[rnd].duration.value) >= MIN_DURATION_SECONDS[intervals[rnd].pace.rawValue-1]*5 + 5) {
                intervals[rnd].duration.value -= 5;
                modified = true;
            }
        } while(!modified)
        return intervals;
    }
    
    private func calcDistanceUserSpeed(intervals: [Interval]) -> Measurement<UnitLength> {
        var distance = 0.0;
        
        for interval in intervals {
            let speed = EXAMPLE_USER_SPEEDS[interval.pace.rawValue];
            distance += speed.value * interval.duration.value;
            distance += EXAMPLE_USER_SPEEDS[0].value * interval.recovery.value;
        }
        return Measurement(value: distance, unit: UnitLength.meters);
    }
    
    private func generateDuration(pace: Pace) -> Measurement<UnitDuration> {
        let rnd = Int.random(in: MIN_DURATION_SECONDS[pace.rawValue-1] ... MAX_DURATION_SECONDS[pace.rawValue-1])
        return Measurement(value: Double(rnd * 5), unit: UnitDuration.seconds)
    }
}
