//
//  IntervalSet.swift
//  Project Sonic
//
//  Created by Peri on 12.11.21.
//

import Foundation

struct IntervalContainer {
    var generatedIntensity: Intensity? = nil
    var generatedDistance: Measurement<UnitLength>? = nil
    public var intervals: [Interval] = []
    
    public func runGenerated(intensity: Intensity, targetDistance: Measurement<UnitLength>) -> Bool {
        return generatedIntensity == intensity && generatedDistance == targetDistance && !intervals.isEmpty
    }
}

public struct IntervalSet {
    private let generator = IntervalGenerator()

    var targetDistance: Measurement<UnitLength> = Measurement(value: 5, unit: UnitLength.kilometers)
    var intensity: Intensity = .Moderate

    var userSpeed: [Measurement<UnitSpeed>]

    private var container: IntervalContainer = IntervalContainer()
    
    var intervals: [Interval] {
        return container.intervals
    }
    
    init(userSpeed: [Measurement<UnitSpeed>]) {
        self.userSpeed = userSpeed
    }
    
    var duration: Measurement<UnitDuration> {
        guard runGenerated else {
            return Measurement(value: 0, unit: UnitDuration.seconds)
        }
        var dur = 0.0
        for interval in intervals {
            dur += interval.duration.converted(to: UnitDuration.seconds).value
        }
        return Measurement(value: dur, unit: UnitDuration.seconds)
    }

    var estimatedDistance: Measurement<UnitLength> {
        targetDistance
    }

    var runGenerated: Bool {
        return container.runGenerated(intensity: intensity, targetDistance: targetDistance);
    }

    mutating func updateContainer(newContainer: IntervalContainer) {
        container = newContainer
    }
    
    func generateNewIntervals() async -> IntervalContainer {
        let generatedIntensity = intensity
        let generatedDistance = targetDistance
        let intervals = await generator.generateRun(intensity: intensity, distance: targetDistance, userPaces: userSpeed)
        return IntervalContainer(generatedIntensity: generatedIntensity, generatedDistance: generatedDistance, intervals: intervals)
    }
}
