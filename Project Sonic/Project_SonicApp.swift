//
//  Project_SonicApp.swift
//  Project Sonic
//
//  Created by Peri on 08.11.21.
//

import SwiftUI

@main
struct Project_SonicApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
