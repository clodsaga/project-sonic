//
//  Pace.swift.swift
//  Project Sonic
//
//  Created by Peri on 10.11.21.
//

import Foundation
import SwiftUI

enum Pace: Int {
    case Recovery = 0, Ten, Five, One, Best
}

let PACE_TEXT = [
    "Recovery",
    "10K",
    "5K",
    "1K",
    "Best Pace",
]

let PACE_COLOR = [
    Color.gray,
    Color.green,
    Color.yellow,
    Color.orange,
    Color.red,
]

extension Pace {
    public var text: String {
        PACE_TEXT[rawValue]
    }

    public var color: Color {
        PACE_COLOR[rawValue]
    }
}
