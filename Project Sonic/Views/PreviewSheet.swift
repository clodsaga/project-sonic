//
//  PreviewSheet.swift
//  Project Sonic
//
//  Created by Peri on 01.12.21.
//

import SwiftUI

let previewIntervals = [
    Interval(pace: .Five, duration: Measurement(value: 69, unit: UnitDuration.seconds),recovery: Measurement(value: 299, unit: UnitDuration.seconds)),
    Interval(pace: .Five, duration: Measurement(value: 299, unit: UnitDuration.seconds), recovery: Measurement(value: 299, unit: UnitDuration.seconds)),
    Interval(pace: .Five, duration: Measurement(value: 15, unit: UnitDuration.seconds), recovery: Measurement(value: 299, unit: UnitDuration.seconds)),
    Interval(pace: .Five, duration: Measurement(value: 60, unit: UnitDuration.seconds), recovery: Measurement(value: 299, unit: UnitDuration.seconds)),
    Interval(pace: .Five, duration: Measurement(value: 90, unit: UnitDuration.seconds), recovery: Measurement(value: 299, unit: UnitDuration.seconds)),
]

struct PreviewSheet: View {
    var intervals: [Interval]
    var regenerate: () async -> Void
    var close: () -> Void
    var runGenerated: Bool
    
    @State private var rotation = 0.0
    @State private var showFrist = true
    @State private var regenerating = false
    
    var body: some View {
        return VStack {
            HStack {
                HStack {
                    Text("Preview").font(.title)
                    
                    Button(action: {
                        Task {
                            rotation += 360.0
                            withAnimation {
                                regenerating = true
                                showFrist.toggle()
                            }
                            
                            let generator = UIImpactFeedbackGenerator(style: .light)
                            generator.prepare()
                            
                            await regenerate()
                            
                            generator.impactOccurred()
                            withAnimation {
                                regenerating = false
                            }
                        }
                    }) {
                        Image(systemName: "arrow.triangle.2.circlepath")
                            .rotationEffect(.degrees(rotation), anchor: .center)
                            .animation(.easeOut(duration: 0.3), value: rotation)
                    }
                    
                    Spacer()
                }
                .padding([.top, .leading, .trailing], 20.0)
                
                Button(role: .cancel, action: close) {
                    Image(systemName: "xmark")
                }
                .foregroundColor(.black)
                .buttonStyle(.bordered)
                .mask {
                    Circle()
                }
                .padding([.top, .trailing], 10.0)
            }
            ZStack {
                List{}
                if showFrist {
                    intervalList(!showFrist)
                } else {
                    intervalList(showFrist)
                }
            }
        }
        .onAppear {
            if !runGenerated {
                Task {
                    regenerating = true
                    await regenerate()
                    regenerating = false
                }
            }
        }
    }
    
    private func intervalList(_ showPreview: Bool) -> some View {
        ZStack {
            if regenerating == false || showPreview {
                IntervalList(intervals: intervals)
            } else {
                PlaceholderIntervalView()
            }
        }
        .transition(.asymmetric(insertion: .move(edge: .bottom), removal: .move(edge: .trailing)))
        .zIndex(showFrist ? 1 : 0)
    }
}

struct PreviewSheet_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            
        }
        .sheet(isPresented: .constant(true)) {
            PreviewSheet(intervals: previewIntervals, regenerate: {}, close: {}, runGenerated: true)
        }
    }
}
