//
//  IntervalOverview.swift
//  Project Sonic
//
//  Created by Peri on 29.11.21.
//

import SwiftUI

struct IntervalOverview: View {
    var intervalSet: IntervalSet
    var body: some View {
        VStack {
            HStack {
                Text("Intesity:")
                IntensityDisplay(intensity: intervalSet.intensity)
                Text(" | ")
                Text("Duration:")
                Text(intervalSet.duration.formatted())
            }
            
            HStack {
                Text("Estimated Distance:")
                Text(intervalSet.estimatedDistance.formatted())
            }
        }
    }
}

struct IntervalOverview_Previews: PreviewProvider {
    static var previews: some View {
        let state = GlobalState()
        state.intervalSet.updateContainer(newContainer: IntervalContainer(generatedIntensity: .Moderate, generatedDistance: Measurement(value: 5.0, unit: UnitLength.meters), intervals: previewIntervals))
        
        return IntervalOverview(intervalSet: state.intervalSet)
            .previewLayout(.sizeThatFits)
    }
}
