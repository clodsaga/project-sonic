//
//  StartView.swift
//  Project Sonic
//
//  Created by Peri on 08.11.21.
//

import SwiftUI
import SheeKit

struct StartView: View {
    @EnvironmentObject var nav: AppNavigation
    @EnvironmentObject var state: GlobalState
    
    @State private var previewVisible = false
    @State private var selectedDetent: UISheetPresentationController.Detent.Identifier?
    
    @State private var showingAlert = false
    @State private var alertMessage = ""
    
    @StateObject private var workoutManager = WorkoutManager()

    var body: some View {
        VStack {
            Spacer()
            
            Text("Project Sonic")
                .font(.largeTitle)

            Spacer()
            
            Text("Intensity")
            IntensitySelect(intensity: $state.intervalSet.intensity)
                .padding(.bottom, 40)

            Text("Distance")
            DistanceSelector(distance: $state.intervalSet.targetDistance)

            HStack {
                BigButton(text: "Preview", icon: "info.circle", action: {
                    selectedDetent = .medium
                    previewVisible.toggle()
                })
                    .buttonStyle(.bordered)
                BigButton(text: "Go", icon: "figure.walk", action: {
                    if !state.intervalSet.runGenerated {
                        await updateIntervals()
                    }
                    let (success, error) = await workoutManager.startWorkout(intervalSet: state.intervalSet)
                    guard success else {
                        alertMessage = error!
                        showingAlert = true
                        return
                    }
                })
                    .buttonStyle(.borderedProminent)
            }
        }
        .scenePadding()
        .shee(
            isPresented: $previewVisible,
            presentationStyle: .popover(
                permittedArrowDirections: .any,
                adaptiveSheetProperties: .init(
                    prefersGrabberVisible: true,
                    detents: [ .medium(), .large() ],
                    selectedDetentIdentifier: $selectedDetent,
                    animatesSelectedDetentIdentifierChange: true,
                    prefersScrollingExpandsWhenScrolledToEdge: false
                )
            )
        ) {
            PreviewSheet(
                intervals: state.intervalSet.intervals,
                regenerate: { await updateIntervals() },
                close: { previewVisible = false},
                runGenerated: state.intervalSet.runGenerated
            )
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Failed to start Run"), message: Text(alertMessage))
        }
    }
    
    private func updateIntervals() async {
        let container = await state.intervalSet.generateNewIntervals()
        state.intervalSet.updateContainer(newContainer: container)
    }
}

struct StartView_Previews: PreviewProvider {
    static var previews: some View {
        let state = GlobalState()
        state.intervalSet.targetDistance = Measurement(value: 5, unit: UnitLength.kilometers)
        state.intervalSet.intensity = .Moderate
        let nav = AppNavigation()
        return StartView()
            .environmentObject(state)
            .environmentObject(nav)
    }
}
