//
//  IntensityDisplay.swift
//  Project Sonic
//
//  Created by Peri on 12.11.21.
//

import SwiftUI

struct IntensityDisplay: View {
    var intensity: Intensity

    var body: some View {
        Text(intensity.text)
    }
}

struct IntensityDisplay_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            IntensityDisplay(intensity: .Recovery)
            IntensityDisplay(intensity: .Light)
            IntensityDisplay(intensity: .Moderate)
            IntensityDisplay(intensity: .High)
        }.previewLayout(.sizeThatFits)
    }
}
