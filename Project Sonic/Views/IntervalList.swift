//
//  IntervalList.swift.swift
//  Project Sonic
//
//  Created by Peri on 29.11.21.
//

import SwiftUI

struct IntervalList: View {
    var intervals: [Interval]
    var body: some View {
        List(intervals) { interval in
            HStack() {
                Text(interval.duration.formatted())
                PaceDisplay(pace: interval.pace)
                Spacer()
                Text(interval.recovery.formatted())
                    .foregroundColor(Pace.Recovery.color)
                PaceDisplay(pace: .Recovery)
            }
        }
    }
}

struct IntervalList_Previews: PreviewProvider {
    static var previews: some View {
        return IntervalList(intervals: previewIntervals)
            .previewLayout(.sizeThatFits)
    }
}
