//
//  DistanceSelector.swift
//  Project Sonic
//
//  Created by Peri on 11.11.21.
//

import SwiftUI

struct DistanceSelector: View {
    @Binding var distance: Measurement<UnitLength>

    private static let distancesArr: [Int] = Array(stride(from: 1000, through: 10000, by: 100))

    let locale = Locale.autoupdatingCurrent

    var body: some View {
        Picker("Distance", selection: $distance) {
            ForEach(DistanceSelector.distancesArr, id: \.self) { i in
                Text(
                    measurementFromMeter(i).formatted(
                        .measurement(
                            width: .wide,
                            usage: .road,
                            numberFormatStyle: .number.precision(
                                .fractionLength(locale.usesMetricSystem ? 1 : 2)
                            )
                        )
                    )
                ).tag(measurementFromMeter(i))
            }
        }
        .pickerStyle(WheelPickerStyle())
    }

    private func measurementFromMeter(_ meters: Int) -> Measurement<UnitLength> {
        Measurement(value: Double(meters), unit: UnitLength.meters)
    }
}

struct DistanceSelector_Previews: PreviewProvider {
    static var previews: some View {
        DistanceSelector(distance: .constant(Measurement(value: 6000, unit: UnitLength.meters)))
            .previewLayout(.sizeThatFits)
    }
}
