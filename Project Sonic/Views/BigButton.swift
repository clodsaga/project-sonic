//
//  GenerateButton.swift
//  Project Sonic
//
//  Created by Peri on 11.11.21.
//

import SwiftUI

struct BigButton: View {
    var text: String
    var icon: String?
    var action: () async -> Void
    
    @State private var loading = false

    var body: some View {
        Button(action: {
            loading = true
            Task {
                await action()
                loading = false
            }
        }) {
            HStack {
                if (icon != nil) {
                    Image(systemName: icon!)
                }
                Text(text)
            }
               .frame(maxWidth: .infinity)
        }
        .disabled(loading)
        .controlSize(.large)
    }
}

struct GenerateButton_Previews: PreviewProvider {
    static var previews: some View {
        BigButton(text: "Test", action: {}).previewLayout(.sizeThatFits)
    }
}
