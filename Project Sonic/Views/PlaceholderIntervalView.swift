//
//  PlaceholderIntervalView.swift
//  Project Sonic
//
//  Created by Peri on 03.12.21.
//

import SwiftUI

struct PlaceholderIntervalView: View {
    var body: some View {
        List(0..<50) { i in
            HStack {
                Text("placeholder")
                Text("placeholder")
                Spacer()
                Text("placeholder")
                PaceDisplay(pace: .Recovery)
            }
        }
    }
}

struct PlaceholderIntervalView_Previews: PreviewProvider {
    static var previews: some View {
        PlaceholderIntervalView()
            .previewLayout(.sizeThatFits)
    }
}
