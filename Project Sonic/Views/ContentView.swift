//
//  ContentView.swift
//  Project Sonic
//
//  Created by Peri on 08.11.21.
//

import SwiftUI

enum AppView {
    case start, run
}

class AppNavigation: ObservableObject {
    @Published var view: AppView = .start

    public func goto(_ target: AppView) {
        withAnimation(.easeOut(duration: 0.2)) {
            view = target
        }
    }
}

class GlobalState: ObservableObject {
    @Published var intervalSet = IntervalSet(userSpeed: EXAMPLE_USER_SPEEDS)
}

struct ContentView: View {
    @StateObject var nav = AppNavigation()
    @StateObject var state = GlobalState()

    var body: some View {
        ZStack {
            switch nav.view {
            case .start:
                StartView()
                    .environmentObject(nav)
                    .environmentObject(state)
            case .run:
                Text("Run")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
