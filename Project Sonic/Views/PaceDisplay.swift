//
//  PaceDisplay.swift
//  Project Sonic
//
//  Created by Peri on 11.11.21.
//

import SwiftUI

struct PaceDisplay: View {
    var pace: Pace
    var body: some View {
        Text(pace.text)
            .foregroundColor(pace.color)
    }
}

struct PaceDisplay_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            PaceDisplay(pace: .Recovery)
            PaceDisplay(pace: .Ten)
            PaceDisplay(pace: .Five)
            PaceDisplay(pace: .One)
            PaceDisplay(pace: .Best)
        }.previewLayout(.sizeThatFits)
    }
}
