//
//  IntensitySelect.swift
//  Project Sonic
//
//  Created by Peri on 10.11.21.
//

import SwiftUI

struct IntensitySelect: View {
    @Binding var intensity: Intensity

    var body: some View {
        VStack {
            Picker("Intensity", selection: $intensity) {
                Text("Recovery").tag(Intensity.Recovery)
                Text("Light").tag(Intensity.Light)
                Text("Moderate").tag(Intensity.Moderate)
                Text("High").tag(Intensity.High)
            }
            .pickerStyle(SegmentedPickerStyle())
        }
    }
}

struct IntensitySelect_Previews: PreviewProvider {
    static var previews: some View {
        let intensity = Intensity.Recovery
        IntensitySelect(intensity: .constant(intensity))
            .previewLayout(.sizeThatFits)
    }
}
