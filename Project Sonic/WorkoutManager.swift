//
//  WorkoutManager.swift
//  Project Sonic
//
//  Created by Peri on 03.12.21.
//

import Foundation
import HealthKit


class WorkoutManager: ObservableObject {
    
    private static let healthStore: HKHealthStore = HKHealthStore()

    public func startWorkout(intervalSet: IntervalSet) async -> (Bool, String?) {
        let (success, authError) = await requestHKPermissions()
        guard success else {
            return (false, authError)
        }
        
        let config = HKWorkoutConfiguration()
        config.locationType = .outdoor
        config.activityType = .running
        
        do {
            try await WorkoutManager.healthStore.startWatchApp(toHandle: config)
            return (true, nil)
        } catch {
            print(error)
            return (false, error.localizedDescription)
        }
    }
    
    public func requestHKPermissions() async -> (Bool, String?) {
        guard HKHealthStore.isHealthDataAvailable() else {
            return (false, "HealthKit unavailable")
        }
        let typesToShare: Set = [
            HKQuantityType.quantityType(forIdentifier: .heartRate)!,
            HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
            HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!,
            HKCategoryType.workoutType()
        ]

        let typesToRead: Set = [
            HKQuantityType.quantityType(forIdentifier: .heartRate)!,
            HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
            HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!,
            HKCategoryType.workoutType()
        ]
        do {
            try await WorkoutManager.healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead)
            return (true, nil)
        } catch {
            return (false, error.localizedDescription)
        }
    }
}
