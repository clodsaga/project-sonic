//
//  Interval.swift
//  Project Sonic
//
//  Created by Peri on 10.11.21.
//

import Foundation

struct Interval: Identifiable {
    var id = UUID()

    var pace: Pace
    var duration: Measurement<UnitDuration>
    
    var recovery: Measurement<UnitDuration>
}
