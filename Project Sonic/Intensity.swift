//
//  Intensity.swift
//  Project Sonic
//
//  Created by Peri on 08.11.21.
//

import Foundation

enum Intensity: Int {
    case Recovery = 0, Light, Moderate, High
}

let INTENSITY_NAMES = [
    "Recovery",
    "Light",
    "Moderate",
    "High",
]
extension Intensity {
    var text: String {
        INTENSITY_NAMES[rawValue]
    }
}
