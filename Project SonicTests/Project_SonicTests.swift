//
//  Project_SonicTests.swift
//  Project SonicTests
//
//  Created by Peri on 08.11.21.
//

@testable import Project_Sonic
import XCTest

class Project_SonicTests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIntervalGenerator() throws {
        let generator = IntervalGenerator()
        let intervals = generator.generateRun(intensity: .Moderate,
                                              distance: Measurement(value: 5, unit: UnitLength.kilometers),
                                              userPaces: [
                                                  Measurement(value: 3, unit: UnitSpeed.metersPerSecond),
                                                  Measurement(value: 4, unit: UnitSpeed.metersPerSecond),
                                                  Measurement(value: 5, unit: UnitSpeed.metersPerSecond),
                                                  Measurement(value: 6, unit: UnitSpeed.metersPerSecond),
                                              ])

        XCTAssertEqual(20, intervals.count)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
}
