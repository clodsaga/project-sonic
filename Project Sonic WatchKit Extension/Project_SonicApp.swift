//
//  Project_SonicApp.swift
//  Project Sonic WatchKit Extension
//
//  Created by Peri on 08.11.21.
//

import SwiftUI

@main
struct Project_SonicApp: App {
    @WKExtensionDelegateAdaptor(WatchkitDelegate.self) var delegate
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
