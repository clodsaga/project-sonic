//
//  NotificationView.swift
//  Project Sonic WatchKit Extension
//
//  Created by Peri on 08.11.21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
