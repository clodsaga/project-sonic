//
//  WatchkitDelegate.swift
//  Project Sonic WatchKit Extension
//
//  Created by Peri on 03.12.21.
//

import Foundation
import WatchKit
import HealthKit


class WatchkitDelegate : NSObject, WKExtensionDelegate {
    
    func applicationDidBecomeActive() {
        print("test")
    }
    
    func handleActiveWorkoutRecovery() {
    }
    
    func handle(_ workoutConfiguration: HKWorkoutConfiguration) {
        print("Starting Workout")
    }
}
